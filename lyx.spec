## lyx-fonts
%global fontname lyx
BuildRequires: fontpackages-devel

%global _without_included_boost --without-included-boost
%global texmf %{_datadir}/texlive/texmf-dist

# Do we need to rebuild configuration files?
%global autotools 0

# Trim changelog to a reasonable size
%global _changelog_trimtime %(date +%s -d "1 year ago")

Name:    lyx
Version: 2.4.3
Release: 1
Summary: WYSIWYM (What You See Is What You Mean) document processor
License: GPL-2.0-or-later
Url:     https://www.lyx.org/
Source0: https://ftp.lip6.fr/pub/lyx/stable/2.4.x/%{name}-%{version}.tar.xz

Source1: lyxrc.dist

# font metainfo file
Source20: %{fontname}.metainfo.xml

%if 0%{?autotools}
BuildRequires: automake libtool
%endif
# weird but necessary to compare the supported qt version
# see http://comments.gmane.org/gmane.editors.lyx.devel/137498
BuildRequires: bc

%if 0%{?_without_included_boost:1}
BuildRequires: boost-devel
%endif

BuildRequires: make
BuildRequires: gcc-c++
BuildRequires: desktop-file-utils
BuildRequires: enchant2-devel
BuildRequires: file-devel
BuildRequires: gettext
BuildRequires: hunspell-devel
BuildRequires: mythes-devel
BuildRequires: python3-devel

BuildRequires: pkgconfig(Qt6Core)
BuildRequires: pkgconfig(Qt6Widgets)
BuildRequires: pkgconfig(Qt6Gui)
BuildRequires: pkgconfig(Qt6Svg)
BuildRequires: libxkbcommon-devel

BuildRequires: texlive-texmf-fonts
BuildRequires: tex(dvips)
BuildRequires: tex(latex)
BuildRequires: zlib-devel

Requires(post): texlive
Requires(postun): texlive
Requires: %{name}-common = %{version}-%{release}
Requires: %{fontname}-fonts = %{version}-%{release}
Requires: hicolor-icon-theme
Requires: python3

Recommends: texlive-collection-latexrecommended

# required for file conversions
Requires: ImageMagick
Requires: xdg-utils
Requires: ghostscript
## produce PDF files directly from DVI files
Requires: tex-dvipdfmx
## convert eps to pdf
Requires: tex-epstopdf
## checking the quality of the generated latex
Requires: tex-chktex
## instant preview
Requires: tex-dtl
Requires: tex(cprotect.sty)
# LaTeX packages required to compile the User's Manual
Requires: tex(dvips)
Requires: tex(esint.map)
Requires: tex(esint.sty)
Requires: tex(latex)
Requires: tex(nomencl.sty)
Requires: tex(simplecv.cls)
Requires: tex(ulem.sty)
Requires: tex(xcolor.sty)

%description
LyX is a modern approach to writing documents which breaks with the
obsolete "typewriter paradigm" of most other document preparation
systems.

It is designed for people who want professional quality output
with a minimum of time and effort, without becoming specialists in
typesetting.

The major innovation in LyX is WYSIWYM (What You See Is What You Mean).
That is, the author focuses on content, not on the details of formatting.
This allows for greater productivity, and leaves the final typesetting
to the backends (like LaTeX) that are specifically designed for the task.

With LyX, the author can concentrate on the contents of his writing,
and let the computer take care of the rest.

%package common
Summary:  Common files of %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch
Requires(postun): /usr/bin/texhash
Requires(posttrans): /usr/bin/texhash
OrderWithRequires(posttrans): /usr/bin/texhash

%description common
%{Summary}.

%package fonts
Summary: Lyx/MathML fonts
# The actual license says "The author of these fonts, Basil K. Malyshev, has
# kindly granted permission to use and modify these fonts."
# One of the font files (wasy10) is separately licensed GPL+.
License: Copyright only and GPL+
Requires: fontpackages-filesystem
Provides:  mathml-fonts = 1.0-50
Provides:  lyx-cmex10-fonts = %{version}-%{release}
Provides:  lyx-cmmi10-fonts = %{version}-%{release}
Provides:  lyx-cmr10-fonts = %{version}-%{release}
Provides:  lyx-cmsy10-fonts = %{version}-%{release}
BuildArch: noarch
%description  fonts
A collection of Math symbol fonts for %{name}.


%prep

%autosetup -p1

# prefer xdg-open over alternatives in configuration
for prog in xv firefox kghostview pdfview xdvi
do
    sed -i -e "s/'$prog'/'xdg-open', '$prog'/" lib/configure.py
done

%if 0%{?autotools}
./autogen.sh
%endif


%build
%configure \
  --disable-dependency-tracking \
  --disable-rpath \
  --disable-silent-rules \
  --enable-build-type=release \
  --enable-optimization="%{optflags}" \
  --without-included-boost \
  --enable-qt6 \
  --with-enchant \
  --with-hunspell

%make_build


%install
%make_install

%py_byte_compile /usr/bin/python3 %{buildroot}%{_datadir}/%{name}/lyx2lyx

# misc/extras
install -p -m644 -D %{S:1} %{buildroot}%{_datadir}/%{name}/lyxrc.dist

# Set up the lyx-specific class files where TeX can see them
mkdir -p %{buildroot}%{texmf}/tex/latex
mv %{buildroot}%{_datadir}/lyx/tex \
   %{buildroot}%{texmf}/tex/latex/lyx

# icon
install -p -D -m644 lib/images/lyx.png \
  %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/lyx.png

install -p -D -m644 lib/images/lyx.svg \
  %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/lyx.svg

# ghost'd files
touch %{buildroot}%{_datadir}/%{name}/lyxrc.defaults
touch %{buildroot}%{_datadir}/%{name}/{packages,textclass}.lst

# fonts
install -m 0755 -d %{buildroot}%{_fontdir}
mv %{buildroot}%{_datadir}/lyx/fonts/*.ttf %{buildroot}%{_fontdir}/
rm -rf %{buildroot}%{_datadir}/lyx/fonts

# Add AppStream metadata
install -Dm 0644 -p %{S:20} \
        %{buildroot}%{_metainfodir}/%{fontname}.metainfo.xml

%find_lang %{name}

# bash completion
install -p -D -m 0644 lib/scripts/bash_completion %{buildroot}%{_datadir}/bash-completion/completions/lyx

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/lyx.desktop
# tests/test_filetools error bogus ( see http://bugzilla.redhat.com/723938 )
make -k check ||:

%postun common
if [ $1 -eq 0 ] ; then
  texhash >& /dev/null
fi

%posttrans common
texhash >& /dev/null

%files
%doc ANNOUNCE lib/CREDITS NEWS README
%license COPYING
%{_bindir}/*

%files common -f %{name}.lang
%{_mandir}/man1/*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/*/*
%dir %{_datadir}/%{name}
%config(noreplace) %{_datadir}/%{name}/lyxrc.dist
%ghost %{_datadir}/%{name}/lyxrc.defaults
%ghost %{_datadir}/%{name}/*.lst
%{_datadir}/%{name}/CREDITS
%{_datadir}/%{name}/RELEASE-NOTES
%{_datadir}/%{name}/autocorrect
%{_datadir}/%{name}/bind
%{_datadir}/%{name}/chkconfig.ltx
%{_datadir}/%{name}/citeengines
%{_datadir}/%{name}/commands
%{_datadir}/%{name}/configure.py
%{_datadir}/%{name}/docbook
%{_datadir}/%{name}/encodings
%{_datadir}/%{name}/examples
%{_datadir}/%{name}/images
%{_datadir}/%{name}/kbd
%{_datadir}/%{name}/languages
%{_datadir}/%{name}/latexfonts
%{_datadir}/%{name}/layouts
%{_datadir}/%{name}/layouttranslations
%{_datadir}/%{name}/lyx2lyx
%{_datadir}/%{name}/scripts
%{_datadir}/%{name}/symbols
%{_datadir}/%{name}/syntax.default
%{_datadir}/%{name}/tabletemplates
%{_datadir}/%{name}/templates
%{_datadir}/%{name}/ui
%{_datadir}/%{name}/unicodesymbols
%{_datadir}/%{name}/xtemplates
%doc %{_datadir}/%{name}/doc
%{texmf}/tex/latex/lyx/
%{_metainfodir}/org.%{name}.LyX.metainfo.xml
%{_datadir}/bash-completion/completions/lyx

%_font_pkg
%{_fontdir}/*.ttf
%license lib/fonts/BaKoMaFontLicense.txt
%{_metainfodir}/%{fontname}.metainfo.xml

%changelog
* Thu Jan 16 2025 Funda Wang <fundawang@yeah.net> - 2.4.3-1
- update to 2.4.3

* Mon May 15 2023 wangtaozhi <wangtaozhi@kylinsec.com.cn> - 2.3.7-1
- Package init
